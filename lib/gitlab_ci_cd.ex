defmodule GitlabCiCd do
  @moduledoc """
  Documentation for GitlabCiCd.
  """

  @doc """
  Hello Dinesh Kaushik.

  ## Examples

      iex> GitlabCiCd.hello()
      :dinesh

  """
  def hello do
    :dinesh
  end
end
